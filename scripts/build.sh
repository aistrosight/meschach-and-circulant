#!/usr/bin/env bash
set -euo pipefail

SELF=$(readlink -f "${BASH_SOURCE[0]}")
cd -- "${SELF%/*/*}"

function show_help()
{
  cat << HELP
USAGE

  ${0##*/} [-d <dest_dir>] [-i] [-p <install_prefix>]

OPTIONS
  
  -d <dest_dir>
    Set the CMake destination directory.

  -i
    Install the targets.

  -p <install_prefix>
    Set the CMake installation prefix.
HELP

  exit "$1"
}

CMAKE_ARGS=()
INSTALL=false
while getopts 'd:ip:' opt
do
  case "$opt" in
    d)
      export DESTDIR=$OPTARG
      ;;
    i)
      INSTALL=true
      ;;
    p)
      CMAKE_ARGS+=("-DCMAKE_INSTALL_PREFIX=$OPTARG")
      ;;
    h)
      show_help 0
      ;;
    *)
      show_help 1
      ;;
  esac
done

mkdir -p build
pushd build
  cmake "${CMAKE_ARGS[@]}" ..
  cmake --build .
  if $INSTALL
  then
    cmake --install .
  fi
popd
