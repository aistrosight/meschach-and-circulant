#!/usr/bin/env bash
set -euo pipefail

# SELF=$(readlink -f "${BASH_SOURCE[0]}")
# cd -- "${SELF%/*/*}"

# https://netlib.org/c/meschach/
# https://github.com/yageek/Meschach
# https://github.com/github0null/mesch (?)
mkdir -p meschach
pushd meschach
  wget -N 'https://netlib.org/c/meschach/meschach'{0..4}'.shar'
  for shar in *.shar
  do
    sh -- "$shar"
  done
popd

# https://people.sc.fsu.edu/~jburkardt/c_src/ranlib/
mkdir -p ranlib
pushd ranlib
  wget -N 'https://people.sc.fsu.edu/~jburkardt/c_src/ranlib/ranlib.'{h,c}
popd

# https://people.sc.fsu.edu/~jburkardt/c_src/rnglib/
mkdir -p rnglib
pushd rnglib
  wget -N 'https://people.sc.fsu.edu/~jburkardt/c_src/rnglib/rnglib.'{h,c}
popd

# http://www.columbia.edu/~ad3217/fbm.html
mkdir -p circulant
pushd circulant
  wget -N 'http://www.columbia.edu/~ad3217/fbm/circulant.'{h,c}
  wget -N 'http://www.columbia.edu/~ad3217/fbm/cov.c'
popd
