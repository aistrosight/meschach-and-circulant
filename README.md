---
title: README
---

# Synopsis

Compile the [Meschach](https://netlib.org/c/meschach/) and [circulant](http://www.columbia.edu/~ad3217/fbm.html) libraries and their dependencies with CMake.

# Build And Install

~~~sh
mkdir build
cd build
cmake ..
cmake --build .
cmake --install .
~~~

# Scripts

## build.sh

A wrapper around the CMake installation and build commands with options for setting the install prefix and destination directory. See `build.sh -h` for details.

## clean.sh

Remove the build directory created by `build.sh`.

## download.sh

Download the source files. Given that these files no longer seem to be updated, the script only serves to note the origin of the files.
