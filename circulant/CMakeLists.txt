message(STATUS "adding circulant library...")
add_library(circulant SHARED circulant.c cov.c)
set_property(TARGET circulant PROPERTY C_STANDARD 90)
target_include_directories(
  circulant PRIVATE
  "${CMAKE_CURRENT_SOURCE_DIR}"
  "${CMAKE_SOURCE_DIR}/meschach"
  "${CMAKE_SOURCE_DIR}/ranlib"
  "${CMAKE_SOURCE_DIR}/rnglib"
  )
target_link_libraries(circulant PRIVATE meschach m ranlib rnglib)
install(TARGETS circulant LIBRARY DESTINATION "${CMAKE_INSTALL_LIBDIR}")
install(FILES circulant.h DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}")
