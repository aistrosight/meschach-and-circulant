message(STATUS "adding rnglib library...")
add_library(rnglib SHARED rnglib.c)
set_property(TARGET rnglib PROPERTY C_STANDARD 90)
target_include_directories(
  rnglib PRIVATE 
  "${CMAKE_CURRENT_SOURCE_DIR}"
  )
install(TARGETS rnglib LIBRARY DESTINATION "${CMAKE_INSTALL_LIBDIR}")
install(FILES rnglib.h DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}")
